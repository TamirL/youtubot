// Requiring our module
import { WebClient, RtmClient, CLIENT_EVENTS } from '@slack/client';
import Promise from 'bluebird';

// process.env.SLACK_TOKEN
export default class SlackBot {
  constructor(token, room) {
    this._webClient = new WebClient(token);
    this._rtmClient = new RtmClient(token);

    Promise.promisifyAll(this._webClient.chat);
    Promise.promisifyAll(this._webClient.users);
    Promise.promisifyAll(this._webClient.im);
    Promise.promisifyAll(this._webClient.channels);

    this.roomName = room;
    this.botName = 'youtubot';
  }

  async initialize() {
    this.room = await this.getIdOfRoom(this.roomName);

    // The client will emit an RTM.AUTHENTICATED event on successful connection, with the `rtm.start` payload
    this._rtmClient.on(CLIENT_EVENTS.RTM.AUTHENTICATED, rtmStartData => {
      console.log(`slack bot RTM logged in as ${rtmStartData.self.name} of team ${rtmStartData.team.name}, but not yet connected to a channel`);
    });

    // you need to wait for the client to fully connect before you can send messages
    this._rtmClient.on(CLIENT_EVENTS.RTM.RTM_CONNECTION_OPENED, () => {
      console.log('slack bot RTM connection opened');
    });

    this._rtmClient.on(CLIENT_EVENTS.RTM.DISCONNECT, () => {
      console.log('disconnected, trying to reconnect');
      this._rtmClient.start();
    });

    this._rtmClient.start();
  }

  async sendMessage(message) {
    return await this._webClient.chat.postMessage(this.room, message);
  }

  async getIdOfRoom(roomName) {
    const capsRoomName = roomName.substr(1).toUpperCase();

    if (roomName.startsWith('@')) {
      const usersResponse = await this._webClient.users.list();
      const imListResponse = await this._webClient.im.list();

      const userId = usersResponse.members.find(x => x.name.toUpperCase() === capsRoomName).id;

      return imListResponse.ims.find(x => x.user.toUpperCase() === userId).id;
    }
    else if (roomName.startsWith('#')) {
      const response = await this._webClient.channels.list();

      return response.channels.find(x => x.name.toUpperCase() === capsRoomName).id;
    }

    throw new Error('Room names must start with # for channels or @ for users');
  }

  onCommand(callback) {
    this._rtmClient.on(CLIENT_EVENTS.RTM.RAW_MESSAGE, data => {
      data = JSON.parse(data);
      if (!data || data.type !== 'message' || (data.username && (data.username.toLowerCase() === this.botName || data.username.toLowerCase() === 'bot'))) {
        return;
      }

      // If no text, return.
      if (!data.text || !data.text.trim()) {
        return;
      }

      console.log('message: ', data.text);

      const [code, ...parameters] = data.text.split(' ').filter(x => x);

      callback({ code, parameters });
    });
  }
}
