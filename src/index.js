require('dotenv').config();

import * as youtubePlayer from './player/youtube-player';
import SlackBot from './communication/bot';
import commands from './commands';

async function start() {
  const bot = new SlackBot(process.env.SLACK_TOKEN, process.env.CHANNEL);

  console.log('slack bot created');

  await bot.initialize();

  console.log('slack bot initialized');

  bot.onCommand(async command => {
    try {
      const selectedCommand = commands.find(x => x.isCommand(command));

      if (!selectedCommand) {
        await bot.sendMessage(`could not find a command for ${command.code}`);

        return;
      }

      await selectedCommand.execute(command, youtubePlayer, bot);
    }
    catch (e) {
      console.error(e);
    }
  });
}

start().catch((...params) => console.error('general error on start', ...params));