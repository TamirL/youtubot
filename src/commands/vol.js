export default {
  getCommandInfo() {
    return {
      code: 'vol [amount=(0-100)]',
      description: 'change volume to an amount between 0-100, if not specified - sets vol amount to 100'
    };
  },
  isCommand(command) {
    return command.code.toLowerCase() === 'vol';
  },
  async execute(command, { setVolume }, bot) {
    const newVolume = await setVolume(command.parameters[0]);

    await bot.sendMessage(`new volume: ${newVolume}`);
  }
};