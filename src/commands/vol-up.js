export default {
  getCommandInfo() {
    return {
      code: 'up',
      description: 'increase volume by 10 units'
    };
  },
  isCommand(command) {
    return command.code.toLowerCase() === 'up';
  },
  async execute(command, { volumeUp }, bot) {
    const newVolume = await volumeUp();

    await bot.sendMessage(`new volume: ${newVolume}`);
  }
};