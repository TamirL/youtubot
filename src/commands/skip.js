export default {
  getCommandInfo() {
    return {
      code: 'next/skip',
      description: 'start playing the next song'
    };
  },
  isCommand(command) {
    return command.code.toLowerCase() === 'next' || command.code.toLowerCase() === 'skip';
  },
  async execute(command, { skip }, bot) {
    await skip();

    await bot.sendMessage('song skipped');
  }
};