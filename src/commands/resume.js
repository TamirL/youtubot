export default {
  getCommandInfo() {
    return {
      code: 'resume',
      description: 'resume playing'
    };
  },
  isCommand(command) {
    return command.code.toLowerCase() === 'resume';
  },
  async execute(command, { resume }, bot) {
    await resume();

    await bot.sendMessage('resuming music');
  }
};