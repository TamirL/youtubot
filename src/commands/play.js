import { isYoutubeLink, stripLinkFormatting } from '../utils';

export default {
  getCommandInfo() {
    return {
      code: '<Youtube Link>',
      description: 'adds a youtube video to the play queue'
    };
  },
  isCommand(command) {
    try {
      return isYoutubeLink(stripLinkFormatting(command.code));
    }
    catch (e) {
      return false;
    }
  },
  async execute(command, { addSong }, bot) {
    const song = await addSong(stripLinkFormatting(command.code));

    await bot.sendMessage(`Added song "${song.metadata.title}" to queue`);
  }
};