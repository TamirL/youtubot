function formatSongsList(list) {
  const currentSongMessage = list.currentSong ? `*current song*: \r\n${list.currentSong.metadata.title}\r\n<${list.currentSong.metadata.iurlmq || list.currentSong.metadata.thumbnail}|thumbnail>` : 'no songs playing at the moment 🙁';
  const queueFormattedList = list.queue.map((song, songIndex) => `${songIndex + 1}. ${song.metadata.title}`).join('\r\n') || 'nothing in queue';
  const QueuedSongsMessage = list.queue.length ? `*queued*:\r\n${queueFormattedList}` : '';
  const autoplaySongMessage = list.autoplayNext ? `*next autoplay song*:\r\n${list.autoplayNext.smallMetadata.title}` : '';

  return `🎵🎶🎵🎶🎵🎶🎵🎶🎵🎶🎵🎶🎵

${currentSongMessage}

${autoplaySongMessage}
${QueuedSongsMessage}`;
}

export default {
  getCommandInfo() {
    return {
      code: 'list',
      description: 'get a list of the current playing status'
    };
  },
  isCommand(command) {
    return command.code.toLowerCase() === 'list';
  },
  async execute(command, { getList }, bot) {
    const songList = await getList();

    await bot.sendMessage(formatSongsList(songList));
  }
};