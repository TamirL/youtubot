export default {
  getCommandInfo() {
    return {
      code: 'clear',
      description: 'stop playing and clear the queue'
    };
  },
  isCommand(command) {
    return command.code.toLowerCase() === 'clear';
  },
  async execute(command, { clear }, bot) {
    await clear();

    await bot.sendMessage('queue cleared');
  }
};