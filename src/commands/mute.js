export default {
  getCommandInfo() {
    return {
      code: 'mute',
      description: 'mutes volume'
    };
  },
  isCommand(command) {
    return command.code.toLowerCase() === 'mute';
  },
  async execute(command, { setVolume }, bot) {
    await setVolume(0);

    await bot.sendMessage('sound muted');
  }
};