import play from './play';
import playlist from './playlist';
import skip from './skip';
import pause from './pause';
import resume from './resume';
import list from './list';
import vol from './vol';
import mute from './mute';
import volUp from './vol-up';
import volDown from './vol-down';
import clear from './clear';
import help from './help';

export default [play, playlist, skip, pause, resume, list, vol, mute, volUp, volDown, clear, help];