import commands from './';

function formatCommands() {
  const stupidGif = 'http://i.imgur.com/MfPE78p.gif';
  const commandList = commands.map(command => {
    const info = command.getCommandInfo();

    return `*${info.code}* - ${info.description}`;
  }).join('\r\n');

  return `${commandList}
<${stupidGif}|Sponsored by Shaked Ferrera>`;
}

export default {
  getCommandInfo() {
    return {
      code: 'help',
      description: 'shows commands'
    };
  },
  isCommand(command) {
    return command.code.toLowerCase() === 'help';
  },
  async execute(command, player, bot) {
    await bot.sendMessage(formatCommands());
  }
};