export default {
  getCommandInfo() {
    return {
      code: 'pause',
      description: 'pause current playing song'
    };
  },
  isCommand(command) {
    return command.code.toLowerCase() === 'pause';
  },
  async execute(command, { pause }, bot) {
    await pause();

    await bot.sendMessage('music paused');
  }
};