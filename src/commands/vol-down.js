export default {
  getCommandInfo() {
    return {
      code: 'down',
      description: 'decrease volume by 10 units'
    };
  },
  isCommand(command) {
    return command.code.toLowerCase() === 'down';
  },
  async execute(command, { volumeDown }, bot) {
    const newVolume = await volumeDown();

    await bot.sendMessage(`new volume: ${newVolume}`);
  }
};