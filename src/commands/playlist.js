import { isYoutubeLink, stripLinkFormatting } from '../utils';

export default {
  getCommandInfo() {
    return {
      code: 'playlist <Youtube Playlist Link>',
      description: 'adds a youtube playlist to the play queue'
    };
  },
  isCommand(command) {
    try {
      return command.code.toLowerCase() === 'playlist' && isYoutubeLink(stripLinkFormatting(command.parameters[0]), true);
    }
    catch (e) {
      return false;
    }
  },
  async execute(command, { addPlaylist }, bot) {
    await addPlaylist(stripLinkFormatting(command.parameters[0]));

    await bot.sendMessage('playlist songs added to queue');
  }
};