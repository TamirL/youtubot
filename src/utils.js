import urlParser from 'url';

export function assertYoutubeLink(url, isPlaylist = false) {
  if (!url) {
    throw new Error('A URLParser was not provided');
  }

  try {
    const { hostname, query } = urlParser.parse(url, true);

    if (!hostname) {
      throw new Error(`This is not a YouTube url: ${url}`);
    }

    if (hostname.includes('youtube')) {
      const requiredProperty = isPlaylist ? query.list || query['amp;list'] : query.v;

      if (!requiredProperty) {
        throw new Error(`This is not a YouTube url: ${url}`);
      }
    }
    else if (isPlaylist || !hostname.includes('youtu.be')) {
      throw new Error(`This is not a YouTube url: ${url}`);
    }
  }
  catch (error) {
    throw new Error(`Invalid url ${url}`);
  }
}

export function isYoutubeLink(url, isPlaylist = false) {
  try {
    assertYoutubeLink(url, isPlaylist);
  }
  catch (e) {
    return false;
  }

  return true;
}

export function getPlaylistIdFromLink(url) {
  if (!url) {
    throw new Error('A URLParser was not provided');
  }

  try {
    const { hostname, query } = urlParser.parse(url, true);

    if (!hostname) {
      throw new Error(`This is not a YouTube url: ${url}`);
    }

    if (!hostname.includes('youtube')) {
      throw new Error(`This is not a YouTube url: ${url}`);
    }

    return query.list || query['amp;list'];
  }
  catch (error) {
    throw new Error(`Invalid url ${url}`);
  }
}

export function stripLinkFormatting(url) {
  if (url && url.startsWith('<')) {
    url = url.slice(1);
  }

  if (url && url.endsWith('>')) {
    url = url.slice(0, -1);
  }

  return url;
}

export function createYoutubeLink(id) {
  return `https://www.youtube.com/watch?v=${id}`;
}

export function isNumeric(n) {
  return !Number.isNaN(parseFloat(n)) && isFinite(n);
}