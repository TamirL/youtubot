import youtubeStream from 'youtube-audio-stream';
import lame from 'lame';
import Speaker from 'speaker';
import Volume from 'pcm-volume';
import { assertYoutubeLink, isNumeric } from '../utils';
import Promise from 'bluebird';

const stateTypes = {
  beforeStart: 'beforeStart',
  paused: 'paused',
  playing: 'playing',
  ended: 'ended'
};

export default class YoutubeSongPlayer {
  constructor(url, volume = 1) {
    assertYoutubeLink(url);

    this.url = url;
    this.volume = volume;
    this.state = stateTypes.beforeStart;
  }

  get isStreamOpen() {
    return this.state === stateTypes.playing || this.state === stateTypes.paused;
  }

  get isPlaying() {
    return this.state === stateTypes.playing;
  }

  play() {
    const self = this;

    return new Promise((resolve, reject) => {
      try {
        this._youtubeStreamable = youtubeStream(this.url);

        const onError = error => {
          console.log('error');
          self.state = stateTypes.ended;
          reject(error);
          console.log('Failed to play YouTube video', error);
        };

        this._youtubeStreamable.once('error', onError);

        this._audioStream = this._youtubeStreamable.pipe(new lame.Decoder());

        this._audioStream.once('error', onError);

        this._audioStream.once('format', function(format) {
          self._volumeStream = new Volume(self.volume);
          self._speakerStream = this.pipe(self._volumeStream).pipe(new Speaker(format));

          self.state = stateTypes.playing;

          self._speakerStream.once('error', onError);

          self.waitToFinish()
            .then(() => {
              self._clean();
            });

          self._audioStream.once('data', () => {
            resolve();
          });
        });
      }
      catch (exception) {
        console.error('error playing YT video', exception);
        reject(exception);
      }
    });
  }

  stop() {
    return new Promise((resolve, reject) => {
      try {
        if (this.isStreamOpen) {
          this._youtubeStreamable.unpipe(this._audioStream);
          this._speakerStream.cork();
          this._audioStream.unpipe(this._speakerStream);
          this._speakerStream.end();

          this._clean();
        }

        resolve();
      }
      catch (e) {
        reject(e);
      }
    });
  }

  pause() {
    if (this.isPlaying) {
      console.log('pausing');
      this._speakerStream.cork();
      this.state = stateTypes.paused;
    }
  }

  resume() {
    if (this.state === stateTypes.paused) {
      console.log('resuming');
      this._speakerStream.uncork();
      this.state = stateTypes.playing;
    }
  }

  setVolume(volume) {
    if (!isNumeric(volume)) {
      volume = 1;
    }

    volume = Number(volume);

    if (volume < 0 || volume > 1) {
      volume = 1;
    }

    if (this._volumeStream) {
      this._volumeStream.setVolume(volume);
    }
  }

  waitToFinish() {
    if (!this._speakerStream) {
      return Promise.resolve();
    }

    return new Promise(resolve => {
      this._speakerStream.once('end', () => {
        resolve();
      });

      this._speakerStream.once('close', () => {
        resolve();
      });
    });
  }

  _clean() {
    this.state = stateTypes.ended;

    if (this._speakerStream) {
      this._speakerStream.destroy();
      this._speakerStream = null;
    }

    if (this._audioStream) {
      this._audioStream.end();
      this._audioStream = null;
    }

    if (this._youtubeStreamable) {
      this._youtubeStreamable.end();
      this._youtubeStreamable = null;
    }
  }
}
