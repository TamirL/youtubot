import YoutubeSongPlayer from './youtube-song-player';
import ytdl from 'ytdl-core';
import getYoutubePlaylistMetadata from 'ytpl';
import { createYoutubeLink, getPlaylistIdFromLink, isNumeric } from '../utils';

const queue = [];
let currentSong;
let volume = 100;
let autoplay = true;
const volumeSteps = 10;

const playedSongs = [];

async function doOnPlaying(action) {
  if (currentSong) {
    try {
      return await action(currentSong);
    }
    catch (e) {
      console.error('error performing action on song', e, currentSong, action);
    }
  }
}

export async function addSong(url) {
  const metadata = await ytdl.getInfo(url);

  const song = {
    url,
    metadata
  };

  queue.push(song);

  return song;
}

export async function addPlaylist(url) {
  const listMetadata = await getYoutubePlaylistMetadata(getPlaylistIdFromLink(url));

  listMetadata.items.forEach(item => {
    queue.push({
      url: item.url_simple,
      metadata: item
    });
  });
}

async function getNextOnAutoplay(song) {
  if (!song || !song.metadata) {
    return null;
  }

  if (!song.metadata.related_videos) {
    song.metadata = await ytdl.getInfo(song.url);
  }

  let selectedSongId = 0;
  let [selectedSong] = song.metadata.related_videos;

  while (selectedSong && (!selectedSong.id || playedSongs.includes(selectedSong.id))) {
    selectedSongId++;
    selectedSong = song.metadata.related_videos[selectedSongId];
  }

  if (!selectedSong) {
    [selectedSong] = song.metadata.related_videos;
  }

  return {
    url: createYoutubeLink(selectedSong.id),
    smallMetadata: selectedSong
  };
}

export async function getList() {
  const autoplayNext = queue.length ? null : (await getNextOnAutoplay(currentSong));

  return {
    currentSong,
    autoplayNext,
    queue
  };
}

export async function skip() {
  return await doOnPlaying(async song => await song.player.stop());
}

export async function clear() {
  queue.length = 0;
  autoplay = false;

  return await skip();
}

export async function pause() {
  return await doOnPlaying(async song => await song.player.pause());
}

export async function resume() {
  return await doOnPlaying(async song => await song.player.resume());
}

export async function setVolume(newVolume = 100) {
  if (!isNumeric(newVolume)) {
    newVolume = 100;
  }

  newVolume = Number(newVolume);

  if (newVolume > 100) {
    newVolume = 100;
  }

  if (newVolume < 0) {
    newVolume = 0;
  }

  console.log(`new vol: ${newVolume}`);

  volume = newVolume;

  // cut down the scale from 0-100 to 0-1
  await doOnPlaying(song => song.player.setVolume(volume / 100));

  return volume;
}

export async function volumeUp() {
  return await setVolume(volume + volumeSteps);
}

export async function volumeDown() {
  return await setVolume(volume - volumeSteps);
}

async function handleQueue() {
  try {
    if (queue.length > 0) {
      const song = queue.shift();

      console.log(`playing      ${song.metadata.title}`);

      song.player = new YoutubeSongPlayer(song.url, volume / 100);

      currentSong = song;

      playedSongs.push(currentSong.metadata.vid);

      await currentSong.player.play();
      await currentSong.player.waitToFinish();

      console.log(`done playing ${currentSong.metadata.title}`);

      if (autoplay && queue.length === 0) {
        await addSong((await getNextOnAutoplay(currentSong)).url);
      }

      currentSong = null;
    }
  }
  catch (e) {
    console.error('error in queue', e);
  }

  setTimeout(() => handleQueue(), 100);
}

handleQueue();
